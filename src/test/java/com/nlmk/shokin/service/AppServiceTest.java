package com.nlmk.shokin.service;

import com.nlmk.shokin.exception.ArgumentNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import static org.junit.jupiter.api.Assertions.*;

class AppServiceTest {

    AppService appService;

    @BeforeEach
    public void setup() {
        appService = new AppService();
    }

    @Test
    void sum() throws ArgumentNotFoundException {
        assertEquals(4, appService.sum("2", "2"));
    }

    @Test
    void sumFirstArgumentNotFound() {
        assertThrows(ArgumentNotFoundException.class, () -> appService.sum("", "1"));
    }

    @Test
    void sumSecondArgumentNotFound() {
        assertThrows(ArgumentNotFoundException.class, () -> appService.sum("1", ""));
    }

    @Test
    void sumWrongArgumentFormat() {
        assertThrows(IllegalArgumentException.class, () -> appService.sum("1qwerty", "3"));
    }

    @Test
    void factorial() throws ArgumentNotFoundException {
        assertEquals(24, appService.factorial("4"));
    }

    @Test
    void factorialArgumentNotFound() {
        assertThrows(ArgumentNotFoundException.class, () -> appService.factorial(""));
    }

    @Test
    void factorialNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> appService.factorial("-4"));
    }

    // 0 1 1 2
    @Test
    void fibonacci() throws ArgumentNotFoundException {
        long[] expected = {0, 1, 1, 2};
        long[] result = appService.fibonacci("4");
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void fibonacciArgumentNotFound() {
        assertThrows(ArgumentNotFoundException.class, () -> appService.fibonacci(""));
    }

    @Test
    void fibonacciNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> appService.fibonacci("-4"));
    }

}