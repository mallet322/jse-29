package com.nlmk.shokin.controller;

import com.nlmk.shokin.exception.ArgumentNotFoundException;
import com.nlmk.shokin.service.AppService;

import java.util.Scanner;
import java.util.logging.Logger;

public class AppController {

    private final Scanner scanner = new Scanner(System.in);
    private static final Logger LOGGER = Logger.getLogger(AppController.class.getName());
    private final AppService appService;

    public AppController(AppService appService) {
        this.appService = appService;
    }

    public void sum() {
        System.out.println("Please, enter first argument:");
        final String firstArg = scanner.nextLine();
        System.out.println("Please, enter second argument:");
        final String secondArg = scanner.nextLine();

        try {
            final long result = appService.sum(firstArg, secondArg);
            System.out.println(result);
        } catch (ArgumentNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public void factorial() {
        System.out.println("Please, enter argument:");
        final String arg = scanner.nextLine();
        try {
            final long result = appService.factorial(arg);
            System.out.println(result);
        } catch (ArgumentNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public void fibonacci() {
        System.out.println("Please, enter argument:");
        final String arg = scanner.nextLine();
        long[] fibArray;
        try {
            fibArray = appService.fibonacci(arg);
            for (long result : fibArray) {
                System.out.println(result + " ");
            }
        } catch (ArgumentNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public void displayError() {
        System.out.println("Error! Invalid argument");
    }

}