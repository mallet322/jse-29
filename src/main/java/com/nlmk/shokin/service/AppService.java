package com.nlmk.shokin.service;

import com.nlmk.shokin.exception.ArgumentNotFoundException;
import org.apache.commons.lang3.math.NumberUtils;

import static com.nlmk.shokin.constant.Const.ARG_NOT_FOUND;

public class AppService {

    public long sum(String arg1, String arg2) throws ArgumentNotFoundException {
        if (arg1 == null || arg1.isEmpty()) {
            throw new ArgumentNotFoundException(ARG_NOT_FOUND);
        }
        if (arg2 == null || arg2.isEmpty()) {
            throw new ArgumentNotFoundException(ARG_NOT_FOUND);
        }

        long i1 = parseToLong(arg1);
        long i2 = parseToLong(arg2);
        return i1 + i2;
    }

    public long factorial(String arg) throws ArgumentNotFoundException {
        if (arg == null || arg.isEmpty()) {
            throw new ArgumentNotFoundException(ARG_NOT_FOUND);
        }
        long num = parseToLong(arg);
        if (num < 0) {
            throw new IllegalArgumentException("Argument < 0");
        }
        long result = 1;
        for (int i = 1; i <= num; i++) {
            result = Math.multiplyExact(result, i);
        }
        return result;
    }

    public long[] fibonacci(String arg) throws ArgumentNotFoundException {
        if (arg == null || arg.isEmpty()) {
            throw new ArgumentNotFoundException(ARG_NOT_FOUND);
        }

        long maxNumber = parseToLong(arg);

        if (maxNumber < 0) {
            throw new IllegalArgumentException("Argument < 0");
        }
        long[] result = new long[(int) maxNumber];

        for (int i = 0; i < maxNumber; i++) {
            if (i == 0) {
                result[i] = i;
                continue;
            }

            if (i == 1 || i == 2) {
                result[i] = 1;
                continue;
            }

            result[i] = result[i - 1] + result[i - 2];
        }
        return result;
    }

    private long parseToLong(String arg) {
        long result = NumberUtils.toLong(arg);
        if (result == 0) {
            throw new IllegalArgumentException("Wrong argument format");
        }
        return result;
    }

}