package com.nlmk.shokin.exception;

public class ArgumentNotFoundException extends Exception {

    public ArgumentNotFoundException(String message) {
        super(message);
    }

}