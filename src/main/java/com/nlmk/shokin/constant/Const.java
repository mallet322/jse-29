package com.nlmk.shokin.constant;

public class Const {

    public static final String SUM = "1";
    public static final String FACTORIAL = "2";
    public static final String FIBONACCI = "3";

    public static final String ARG_NOT_FOUND = "Argument not found";

}