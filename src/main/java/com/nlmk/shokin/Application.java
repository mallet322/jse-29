package com.nlmk.shokin;

import com.nlmk.shokin.controller.AppController;
import com.nlmk.shokin.service.AppService;

import java.util.Scanner;

import static com.nlmk.shokin.constant.Const.*;

public class Application {

    private final AppService service = new AppService();
    private final AppController controller = new AppController(service);

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application app = new Application();
        String inputString = scanner.nextLine();
        app.run(inputString);
    }

    public void run(String args) {
        if (args == null || args.isEmpty()) return;
        switch (args) {
            case SUM:
                controller.sum();
                return;
            case FACTORIAL:
                controller.factorial();
                return;
            case FIBONACCI:
                controller.fibonacci();
                return;
            default:
                controller.displayError();
        }
    }

}